﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using KSP.IO;

namespace KerbinTestingSite
{
    [KSPAddon(KSPAddon.Startup.MainMenu, true)]
    public class TestModule : MonoBehaviour
    {
        private GameScenes currentScene;
        private int guiSpamCount = 0;
        private int updateSpamCount = 0;

        private bool flightReady = false;
        private int vesselCount = 0;

        public TestModule()
        {
            UpdateCurrentScene();
            print("KTS: new TestModule() SCENE: " + currentScene.ToString());
        }

        private void Awake()
        {
            DontDestroyOnLoad(this); // Prevent this module from being unloaded on scene changes.

            GameEvents.onGamePause.Add(new EventVoid.OnEvent(this.OnPause));
            GameEvents.onGameUnpause.Add(new EventVoid.OnEvent(this.OnUnpause));
            GameEvents.onFlightReady.Add(new EventVoid.OnEvent(this.OnFlightReady));
            GameEvents.onVesselSituationChange.Add(new EventData<GameEvents.HostedFromToAction<Vessel, Vessel.Situations>>.OnEvent(this.OnVesselSituationChange));
            GameEvents.onTimeWarpRateChanged.Add(new EventVoid.OnEvent(this.OnTimeWarpRateChanged));
            

            UpdateCurrentScene();
            print("KTS: Awake() SCENE: " + currentScene.ToString());
        }

        private void Start()
        {
            UpdateCurrentScene();
            print("KTS: Start() SCENE: " + currentScene.ToString());
        }

        private void Update()
        {
            // Limited output so as not to completely spam the logs, as this is called quite often.
            updateSpamCount++;
            UpdateCurrentScene();
            if (updateSpamCount % 300 == 0)
            {
                print("KTS: Update() SCENE: " + currentScene.ToString());
            }

            bool currentFlightReady = FlightGlobals.ready;

            if (currentFlightReady != flightReady)
            {
                print("KTS: Update() Flight ready changed to: " + currentFlightReady);
                flightReady = currentFlightReady;
            }


            if (FlightGlobals.Vessels != null)
            {
                int currentVesselCount = FlightGlobals.Vessels.Count;
                if (currentVesselCount != vesselCount)
                {
                    print("KTS: Update() Vessel count changed to: " + currentVesselCount);
                    vesselCount = currentVesselCount;
                }
            }
        }

        private void OnGUI()
        {
            // Limited output so as not to completely spam the logs, as this is called quite often.
            guiSpamCount++;
            UpdateCurrentScene();
            if (guiSpamCount % 300 == 0) print("KTS: OnGUI() SCENE: " + currentScene.ToString());
        }

        private void OnDestroy()
        {
            print("KTS: OnDestroy() Changing from " + currentScene.ToString() + " to " + HighLogic.LoadedScene.ToString());
            UpdateCurrentScene();

            // Clean up registered event handlers.
            GameEvents.onGamePause.Remove(new EventVoid.OnEvent(this.OnPause));
            GameEvents.onGameUnpause.Remove(new EventVoid.OnEvent(this.OnUnpause));
            GameEvents.onFlightReady.Remove(new EventVoid.OnEvent(this.OnFlightReady));
            GameEvents.onVesselSituationChange.Remove(new EventData<GameEvents.HostedFromToAction<Vessel, Vessel.Situations>>.OnEvent(this.OnVesselSituationChange));
            GameEvents.onTimeWarpRateChanged.Remove(new EventVoid.OnEvent(this.OnTimeWarpRateChanged));
        }

        private void UpdateCurrentScene()
        {
            currentScene = HighLogic.LoadedScene;
        }
        
        private void OnUnpause()
        {
            UpdateCurrentScene();
            print("KTS: OnUnpause() SCENE: " + currentScene.ToString());
        }

        private void OnPause()
        {
            UpdateCurrentScene();
            print("KTS: OnPause() SCENE: " + currentScene.ToString());
        }

        private void OnFlightReady()
        {
            UpdateCurrentScene();
            print("KTS: OnFlightReady() ready = " + FlightGlobals.ready + " SCENE: " + currentScene.ToString());
        }

        private void OnVesselSituationChange(GameEvents.HostedFromToAction<Vessel, Vessel.Situations> data)
        {
            UpdateCurrentScene();
            print("KTS: OnVesselSituationChange() Vessel " + data.host.name + " changed from " + data.from + " to " + data.to + " SCENE: " + currentScene.ToString());
        }

        private void OnTimeWarpRateChanged()
        {
            UpdateCurrentScene();
            print("KTS: OnTimeWarpRateChanged() " + (TimeWarp.WarpMode == TimeWarp.Modes.HIGH ? "Regular" : "Physical") + " time warp changing to " + GetTargetWarpRate() + "x SCENE: " + currentScene.ToString());
        }

        private float GetTargetWarpRate()
        {
            int warpIndex = TimeWarp.CurrentRateIndex;
            if (TimeWarp.WarpMode == TimeWarp.Modes.HIGH)
            {
                // Regular time warp.
                return TimeWarp.fetch.warpRates[warpIndex];
            }
            else
            {
                // Physical time warp.
                return TimeWarp.fetch.physicsWarpRates[warpIndex];
            }
        }

    }
}
